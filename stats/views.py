import re
import datetime
import simplejson as json
from django.db.models import Sum
from django.views.generic import ListView
from django.views.generic.edit import FormView
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
from stats.models import Statistic, Unit, ApiKey
from stats.forms import StatForm, SummaryStatForm
from django.views.decorators.csrf import csrf_exempt

class JSONResponseMixin(object):
    def render_to_response(self, context):
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        return HttpResponse(content, content_type='application/json; charset=utf-8', **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context['object_list'])

class JSONifNoFormResponseMixin(object):
    def render_to_response(self, context):
        if 'form' in context:
            return super(JSONifNoFormResponseMixin, self).render_to_response(context)
        return self.get_json_response(self.convert_context_to_json(context))

    def get_json_response(self, content, **httpresponse_kwargs):
        return HttpResponse(content, content_type='application/json; charset=utf-8', **httpresponse_kwargs)

    def convert_context_to_json(self, context):
        return json.dumps(context['object_list'])

api_key_regex = re.compile(r'^[a-z0-9]+$', re.I)

class ApiKeyAccess(object):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        if 'HTTP_APIKEY' in request.META:
            if api_key_regex.match(request.META['HTTP_APIKEY']):
                key = get_object_or_404(ApiKey, key=request.META['HTTP_APIKEY'])
                if not key.is_expired():
                    key.last_access_ip = request.META['REMOTE_ADDR']
                    key.last_access_date = datetime.datetime.now()
                    key.save()
                    return super(ApiKeyAccess, self).dispatch(request, *args, **kwargs)
        raise Http404

class UnitsView(ApiKeyAccess, JSONResponseMixin, ListView):
    model = Unit

    def get_queryset(self):
        #return [(n.name, n.tag) for n in Unit.objects.all()]
        return [(n.pk, n.name) for n in Unit.objects.all()]

class StatView(ApiKeyAccess, JSONifNoFormResponseMixin, FormView):
    form_class = StatForm
    template_name = 'stat_view.html'

    def form_valid(self, form):
        stat = Statistic.objects.filter(unit=form.cleaned_data.get('unit'),
                        date__gte=form.cleaned_data.get('date_from'),
                        date__lte=form.cleaned_data.get('date_to')).order_by('date')
        #return self.render_to_response({'object_list': [(n.date.strftime("%Y-%m-%d %H:%M"), n.bytes_in) for n in stat]})
        return self.render_to_response({'object_list': [(n.date.strftime("%s"),
                "%.2f" % (float(n.bytes_in)/1024/1024),
                "%.2f" % (float(n.bytes_out)/1024/1024)) for n in stat]})


class SummaryStatView(ApiKeyAccess, JSONifNoFormResponseMixin, FormView):
    form_class = SummaryStatForm
    template_name = 'stat_view.html'

    def form_valid(self, form):
        if form.cleaned_data.get('per_day', False):
            stat = Statistic.objects.filter(date__gte=form.cleaned_data.get('date_from'),
                    date__lte=form.cleaned_data.get('date_to')).order_by('date')

            units = Unit.objects.filter(enabled=True)
            data = []
            for u in units:
                #data.append(stat.filter(unit=u))
                #data.append({'unit': u.name, 'data': ["%.2f" % (float(n.bytes_in)/1024/1024) for n in stat.filter(unit=u)]})
                data.append([u.name] + ["%.2f" % (float(n.bytes_in)/1024/1024) for n in stat.filter(unit=u)]) #.insert(0, u.name))
            return self.render_to_response({'object_list': [[n.date.strftime("%Y-%m-%d %H:%M") for n in stat.filter(unit=u)],data]})
        else:
            date_from = form.cleaned_data.get('date_from')
            date_to = form.cleaned_data.get('date_to')
            stat = Statistic.objects.filter(date__gte=date_from,
                    date__lte=date_to).values('unit__name').annotate(bytes=Sum('bytes_in'))
            return self.render_to_response({'object_list': [['Bytes In (Mb)'],[(n['unit__name'], "%.2f" % (float(n['bytes'])/1024/1024)) for n in stat]],
                    'date_from': date_from, 'date_to': date_to})




