import datetime
from django.db import models

class NetMask(models.Model):
    mask = models.CharField(max_length=2)
    description = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.mask

class Unit(models.Model):
    name = models.CharField(max_length=250)
    #tag = models.PositiveIntegerField(editable=False)
    description = models.CharField(max_length=250, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

    @property
    def tag(self):
        return hex(int(self.pk)+1000)

class Net(models.Model):
    ip = models.IPAddressField()
    mask = models.ForeignKey(NetMask)
    unit = models.ForeignKey(Unit)
    description = models.CharField(max_length=250, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s/%s" % (self.ip, self.mask)

class FlowFile(models.Model):
    name = models.CharField(max_length=100, unique=True, db_index=True)

    def __unicode__(self):
        return self.name

class Conf(models.Model):
    name = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.name


class Statistic(models.Model):
    date = models.DateTimeField(auto_now=False, auto_now_add=False, db_index=True)
    unit = models.ForeignKey(Unit, db_index=True)
    bytes_in = models.CharField(max_length=100, null=True, blank=True, default='0')
    bytes_out = models.CharField(max_length=100, null=True, blank=True, default='0')
    packets_in = models.CharField(max_length=100, null=True, blank=True, default='0')
    packets_out = models.CharField(max_length=100, null=True, blank=True, default='0')

    def __unicode__(self):
        return "%s %s" % (self.date.strftime("%Y-%m-%d %H:%M"), self.unit.name)

    class Meta:
        unique_together = (('date', 'unit'))


class ApiKey(models.Model):
    key = models.CharField(max_length=100, unique=True)
    valid = models.DateTimeField(auto_now=False, auto_now_add=False)
    last_access_ip = models.CharField(max_length=50, null=True, blank=True)
    last_access_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return self.key


    def is_expired(self):
        return datetime.datetime.now() > self.valid


