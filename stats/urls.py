from django.conf.urls import patterns, include, url
from stats.views import UnitsView, StatView, SummaryStatView

urlpatterns = patterns('',
    url(r'^units/', UnitsView.as_view()),
    url(r'^view/', StatView.as_view()),
    url(r'^summary/', SummaryStatView.as_view()),
)

