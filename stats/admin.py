from django.contrib import admin
from stats.models import Net, NetMask, Unit, Conf, Statistic, FlowFile, ApiKey

class NetInline(admin.TabularInline):
    model = Net

class UnitAdmin(admin.ModelAdmin):
    inlines = [
        NetInline,
    ]

class NetAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'unit', 'enabled')
    list_filter = ('unit',)

class ConfAdmin(admin.ModelAdmin):
    list_display = ('name', 'value')

class FlowFileAdmin(admin.ModelAdmin):
    list_per_page = 25

class StatisticAdmin(admin.ModelAdmin):
    list_per_page = 25

class ApiKeyAdmin(admin.ModelAdmin):
    list_display = ('key', 'valid', 'last_access_ip', 'last_access_date', 'enabled')

admin.site.register(Net, NetAdmin)
admin.site.register(NetMask)
admin.site.register(Unit, UnitAdmin)
admin.site.register(Conf, ConfAdmin)
admin.site.register(Statistic, StatisticAdmin)
admin.site.register(FlowFile, FlowFileAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)

