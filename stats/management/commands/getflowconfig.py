from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from pytils import translit
from stats.models import Net, Unit

class Command(BaseCommand):
    def handle(self, *args, **options):
        nets = Net.objects.filter(enabled=True, unit__enabled=True)
        #for net in nets:
            #print "%s %s %s" % (net, net.unit, net.unit.tag)
        rendered = render_to_string('flow_tag.cfg', {'nets': nets })
        print "#>>>>>>>>>>>>>>>>>> Config >>>>>>>>>>>>>>>>\n"
        print rendered

        print "#>>>>>>>>>>>>>>>>> Tag names >>>>>>>>>>>>>>\n"
        units = Unit.objects.filter(enabled=True)
        for unit in units:
            print "%s %s" % (unit.tag, translit.translify(unit.name))

