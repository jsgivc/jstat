import os
import re
import sys
import hashlib
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string
from pytils import translit
from stats.models import Net, Unit, Conf, Statistic, FlowFile
from django.conf import settings
from subprocess import Popen, PIPE
import logging
import logging.handlers
from django.core.mail import send_mail

log = logging.getLogger(__name__)
#log.setLevel(logging.DEBUG)
handler = logging.handlers.SysLogHandler(address = '/dev/log')
log.addHandler(handler)

def send_alert(msg):
    send_mail('Django error on host', msg, 'from@examole.com', ['to@example.com'], fail_silently=False)

def log_error(msg, rtval):
    logging.error(msg)
    send_alert(msg)
    sys.exit(rtval)

def write_cfg(path, data):
    try:
        with open(path, 'w') as f:
            f.write(data)
    except:
        log_error('Cannot write tag cfg file', 1)

def read_cfg(path):
    conf_content = None
    try:
        with open(path, 'r') as f:
            conf_content = f.read()
    except:
        log_error('Cannot read tag cfg file', 2)
    return conf_content

def r2dt(data):
    return datetime.datetime(year=int(data['year']), month=int(data['month']),
        day=int(data['day']), hour=int(data['hour']), minute=int(data['minute']))

def get_cmd(path, tag_conf, tag_def, stat_fmt):
    return '/usr/bin/flow-cat %s | /usr/bin/flow-tag -t %s -T %s | /usr/bin/flow-stat -f %s | grep -v ^#' % (
            path, tag_conf, tag_def, stat_fmt)

cmd_out_regex = re.compile(r'(\d+)\s+(\d+)\s+(\d+)\s+(\d+).*$', re.M)
def get_stat(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    #[(hex(int(n[0])), n[1:]) for n in a]
    #return cmd_out_regex.findall(p.stdout.read())
    return dict([(hex(int(n[0])), n[1:]) for n in cmd_out_regex.findall(p.stdout.read()) if int(n[0]) > 0])


class Command(BaseCommand):
    def handle(self, *args, **options):
        conf_content = None
        nets = Net.objects.filter(enabled=True, unit__enabled=True)
        if not nets:
            log_error('Cannot find nets', 3)

        units = dict([(n.tag, n) for n in  Unit.objects.filter(enabled=True)])
        if not units:
            log_error('Cannot find units', 3)

        rendered = render_to_string('flow_tag.cfg', {'nets': nets })

        flow_tag_cfg = getattr(settings, 'FLOW_TAG_CONFIG', '/tmp/flow_tag.cfg')

        if os.path.exists(flow_tag_cfg):
            conf_content = read_cfg(flow_tag_cfg)

        if conf_content is None or hashlib.md5(rendered).hexdigest() != hashlib.md5(conf_content).hexdigest():
            write_cfg(flow_tag_cfg, rendered)
            logging.warning('Flow tag cfg is changed. Write new config.')

        old_pid, created = Conf.objects.get_or_create(name='old_pid')

        if not created:
            if old_pid.value:
                pgid = None
                try:
                    pgid = os.getpgid(int(old_pid.value))
                except:
                    pass
                if pgid:
                    log_error('Previous proccess are not end work. Exit', 4)

        old_pid.value = os.getpid()
        old_pid.save()


        work_dt = None
        work_dt_obj, created = Conf.objects.get_or_create(name='work_day')
        if created:
            work_dt = datetime.datetime.now().date()
        else:
            wdt_regex = re.compile(r'^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})$')
            if work_dt_obj.value is None:
                work_dt = datetime.datetime.now().date()
            elif wdt_regex.match(work_dt_obj.value):
                parts = wdt_regex.match(work_dt_obj.value).groupdict()
                work_dt = datetime.date(year=int(parts['year']), month=int(parts['month']), day=int(parts['day']))
            else:
                log_error('Cannot determine work datetime.', 5)

        work_path = os.path.join(getattr(settings, 'FLOW_FILES_PATH', '/tmp/sdfsdfsdff_zz'),
                work_dt.strftime('%Y'), work_dt.strftime('%Y-%m'),
                work_dt.strftime('%Y-%m-%d'))

        if not os.path.exists(work_path):
            log_error('Work path does not exists', 6)


        flow_file_regex = re.compile(r'^ft-v05\.(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})\.(?P<hour>\d{2})(?P<minute>\d{2}).*')
        #re.compile(r'(?P<tag>\d+)\s+(?P<flows>\d+)\s+(?P<octets>\d+)\s+(?P<packets>\d+).*$', re.M)
        for dirname, dirnames, filenames in os.walk(work_path):
            for filename in sorted(filenames):
                ff = flow_file_regex.match(filename)
                if ff:
                    if FlowFile.objects.filter(name=filename):
                        continue
                    flow_file_dt =  r2dt(ff.groupdict())
                    stat_in_cmd = get_cmd(os.path.join(work_path, filename), flow_tag_cfg, 'to_nets', '31')
                    stat_out_cmd = get_cmd(os.path.join(work_path, filename), flow_tag_cfg, 'from_nets', '30')
                    stat_in = get_stat(stat_in_cmd)
                    stat_out = get_stat(stat_out_cmd)
                    for u in units:
                        stat_in_data = stat_in[u][1:] if u in stat_in else (0, 0)
                        stat_out_data = stat_out[u][1:] if u in stat_out else (0, 0)
                        stat, created = Statistic.objects.get_or_create(date=flow_file_dt, unit=units[u])
                        stat.bytes_in, stat.packets_in = stat_in_data
                        stat.bytes_out, stat.packets_out = stat_out_data
                        stat.save()
                    flowfile_obj = FlowFile.objects.create(name=filename)
                    flowfile_obj.save()
                    logging.warning('Inserted file %s to database' % (filename))
                    #print units
                    #print ''
                    #print stat_in
                    #print ''
                    #print stat_out

                    #break
                    #stat_obj, created = Statistic



        if datetime.datetime.now().date() > work_dt:
            work_dt_obj.value = (work_dt + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
            FlowFile.objects.all().delete()
        else:
            work_dt_obj.value = datetime.datetime.now().strftime('%Y-%m-%d')


        work_dt_obj.save()

        old_pid.value = ''
        old_pid.save()


