import datetime
from django import forms
from django.forms.models import fields_for_model
from stats.models import Unit, Statistic

class GenericStatForm(forms.Form):
    date_from = forms.DateTimeField(required=False, initial=(datetime.datetime.now()-datetime.timedelta(days=1)))
    date_to = forms.DateTimeField(required=False, initial=datetime.datetime.now())

    def clean_date_from(self):
        date_from = self.cleaned_data.get('date_from', None)
        return date_from if date_from else datetime.datetime.now()

    def clean_date_to(self):
        date_to = self.cleaned_data.get('date_to', None)
        return date_to if date_to else datetime.datetime.now() - datetime.timedelta(days=1)

class StatForm(GenericStatForm):
    _statistic_fields = fields_for_model(Statistic)
    unit = _statistic_fields['unit']


class SummaryStatForm(GenericStatForm):
    per_day = forms.BooleanField(required=False)
